# CHANGELOG



## v0.6.0 (2024-04-29)

### Feature

* feat(sr): adding test ([`7fa4e1d`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/7fa4e1d4398104b6ad97c0fb524b8cd9357e60f1))

* feat(sr): add file ([`5eb8de5`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/5eb8de56c663b105d559bdab8165cebe83c8ff06))

* feat(sr): add files ([`a799214`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/a799214bc69d2e1b2536f226a4a191e4c9d69de8))

### Unknown

* Merge branch &#39;main&#39; of https://gitlab.com/SantiagoRomanoOddone/sr_test into main ([`8481359`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/84813595fb9c56c5fc981855e9ace4c25e751a5d))


## v0.5.0 (2024-04-29)

### Feature

* feat(sr): adding code ([`4334a18`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/4334a18e20a6188227c4057854edaa9c620c0fca))

### Unknown

* Merge branch &#39;main&#39; of https://gitlab.com/SantiagoRomanoOddone/sr_test into main ([`178691a`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/178691a5e10537ef84e99a2c5de13cb5e05141ce))


## v0.4.0 (2024-04-29)

### Feature

* feat(sr): adding code ([`5811996`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/5811996eef4c5006de2a84a903546eceb150e8b2))

### Unknown

* Merge branch &#39;main&#39; of https://gitlab.com/SantiagoRomanoOddone/sr_test into main ([`74144ca`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/74144ca8c1a9089d113f7f543d68085610954918))


## v0.3.0 (2024-04-29)

### Feature

* feat(sr): add file ([`8d0f789`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/8d0f7898007f1e081704f33aad25b611f78e32b5))


## v0.2.0 (2024-04-26)

### Feature

* feat(sr): add semantic release cd pipeline ([`521f0b7`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/521f0b7766396351f8ba05b65f351783a740961d))

* feat(sr): add semantic release cd pipeline ([`38a391f`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/38a391f748a011f35b4f6a7ca9c8f2c5e62225af))

### Unknown

* Merge branch &#39;main&#39; of https://gitlab.com/SantiagoRomanoOddone/sr_test ([`94dd6af`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/94dd6af69eda9035590c8fec55598937b5a6cfdc))


## v0.1.0 (2024-04-26)

### Feature

* feat(sr): add semantic release cd pipeline ([`92d557d`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/92d557d50731db8d2574248f57f100240db1721f))

* feat(sr): add semantic release cd pipeline ([`cdca209`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/cdca2094c4b450c5499618c51f73d6ffefc0e9b9))

* feat(sr): add semantic release cd pipeline ([`e5d43cf`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/e5d43cf490485329768592c1d14f56424dd135af))

* feat(sr): add semantic release cd pipeline ([`249e0b4`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/249e0b4308cbce8e6ddbbacd910c987a38f2973e))

* feat(sr): add semantic release cd pipeline ([`f301767`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/f301767adec0b82cc9d91c7fbfdcebce10ae5a0c))

### Unknown

* Initial commit ([`1e6123b`](https://gitlab.com/SantiagoRomanoOddone/sr_test/-/commit/1e6123bbc2444a40dc651a6a636b331cf1278ed6))
